import java.util.Scanner;

/**
 * PlayListTrack
 *
 * A simple interface for music tracks.
 *
 * @author Abilash Bodapati
 * @version 20190215
 *
 */

public class SimpleMusicTrack implements PlayListTrack {

    // Private class variabels
    private String track;
    private String artist;
    private String album;

    // Class Constructor
    public SimpleMusicTrack() {
        this.track = "";
        this.artist = "";
        this.album = "";
    }

    // Getter method of track name
    @Override
    public String getName() {
        return this.track;
    }

    // Setter method of track name
    @Override
    public void setName(String name) {
        this.track = name;
    }

    // Getter method of track artist
    @Override
    public String getArtist() {
        return this.artist;
    }

    // Setter method of track artist
    @Override
    public void setArtist(String artist) {
        this.artist = artist;
    }

    // Getter method of track album
    @Override
    public String getAlbum() {
        return this.album;
    }

    // Setter method of track album
    @Override
    public void setAlbum(String album) {
        this.album = album;
    }

    // Overriding equals method from Object class for our project purposes
    @Override
    public boolean equals(Object obj) {
        boolean result = false;

        // Check if the object passed is of same type
        if (obj instanceof SimpleMusicTrack) {
            // Cast object
            SimpleMusicTrack compare = (SimpleMusicTrack) obj;

            // if-else statement to check if track info matches
            if (this.track.equals(compare.track)
                    && (this.artist.equals(compare.artist))
                    && (this.album.equals(compare.album))) {
                result = true;
            } else {
                result = false;
            }
        }
        return result;
    }

    // Overriding toString method from Object class for our project purposes
    @Override
    public String toString() {
        String song = "'" + this.artist + " / " + this.track + "'";

        return song;
    }

    /*
     * Attempts to read a playlist track entry from a Scanner object Sets the
     * values in the object to the values given in the file If it successfully
     * loads the track, return true otherwise, return false
     */
    @Override
    public boolean getNextTrack(Scanner infile) {

        // Boolean variable
        boolean loadComplete;

        // Surround code with try-catch to avoid i/o errors
        try {
            // Initialize boolean variable
            loadComplete = false;

            // Initialize track information
            String track = "";
            String artist = "";
            String album = "";

            // while-loop to check for only the next track information
            int i = 0;
            while ((infile.hasNext()) && (((track.equals(""))
                    || (artist.equals("")) || (album.equals("")))) && (i < 3)) {

                // if-else if-else statement to collect info from the file and set equal to the string
                if (i == 0) {
                    track = infile.nextLine();
                } else if (i == 1) {
                    artist = infile.nextLine();
                } else if (i == 2) {
                    album = infile.nextLine();
                }
                i++;
            }

            // If we were able to get all the values from the file for the info then load was a success
            if (((!track.equals("")) || (!artist.equals(""))
                    || (!album.equals("")))) {
                loadComplete = true;
            }
        } catch (Exception e) {
            // Set boolean variable false when file is not found.
            loadComplete = false;
        }
        return loadComplete;
    }
}