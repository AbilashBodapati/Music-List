import java.util.ArrayList;

/**
 * SimplePlayList
 *
 * A simple class for music play lists.
 *
 * @author Abilash Bodapati
 * @version 20190215
 */
public class SimplePlayList implements PlayList {

    //private Queue<PlayListTrack> playTrack;
    private ArrayList<PlayListTrack> playTrack;

    public SimplePlayList() {
        // Create a new ArrayList
        this.playTrack = new ArrayList<>();

    }

    // Removes track from PlayList and returns it to the caller
    @Override
    public PlayListTrack getNextTrack() {
        // Create a SimpleMusicTrack object of type PlayListTrack
        PlayListTrack nextTrack = new SimpleMusicTrack();

        // if-else statements to remove the first element.
        if (this.playTrack.size() > 0) {
            nextTrack = this.playTrack.remove(0);
        } else {
            nextTrack = null;
        }
        return nextTrack;
    }

    // Returns next entry to the caller, but leaves it in the queue
    @Override
    public PlayListTrack peekAtNextTrack() {
        // Create a SimpleMusicTrack object of type PlayListTrack
        PlayListTrack nextTrack = new SimpleMusicTrack();

        // if-else statements to get but not remove the first element.
        if (this.playTrack.size() > 0) {
            nextTrack = this.playTrack.get(0);
        } else {
            nextTrack = null;
        }
        return nextTrack;
    }

    // Adds this track to the playlist in the appropriate order
    @Override
    public void addTrack(PlayListTrack track) {
        this.playTrack.add(track);

    }

    // Returns true if the playlist is empty
    @Override
    public boolean isEmpty() {
        return (this.playTrack.size() == 0);
    }
}