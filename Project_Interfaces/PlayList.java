/**
 * PlayList
 *
 * A simple interface for music play lists of Java classes.
 *
 * @author Jeremy Morris
 * @version 20120823
 */
public interface PlayList {

    // Removes track from PlayList and returns it to the caller
    // Should return a null value if the PlayList is empty
    public PlayListTrack getNextTrack();

    // Returns next entry to the caller, but leaves it in the list
    public PlayListTrack peekAtNextTrack();

    // Adds this track to the playlist in the appropriate order
    public void addTrack(PlayListTrack track);

    // Returns true if the playlist is empty
    public boolean isEmpty();
}