import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * This is a test java file to check if the class SimpleMusicTrack works
 * properly.
 *
 * @author Abilash Bodapati
 * @version 20190215
 *
 */
public class Test_SimpleMusicTrack {

    /**
     * Void method to print out the tracks and its info from the file.
     *
     * @param fileReader
     */
    public static void createTrackFromFile(Scanner fileReader) {

        // Header
        System.out.println("\n" + "Music Tracks from the File");
        System.out.println("--------------------------------");

        // While loop to help print out each track
        int i = 1;
        while (fileReader.hasNext()) {
            System.out.println(fileReader.nextLine());

            if (i == 3) {
                System.out.println("");
                i = 0;
            }

            i++;
        }

        // End note of the sub-test
        System.out.println("End of tracks from the file");
        System.out.println("-------------------------------------" + "\n");

    }

    /**
     * Using a SimpleMusicTrack class to create an ArrayList of tracks from the
     * file.
     *
     * @param fileReader
     * @return ArrayList of SimpleMusicTrack
     */
    public static ArrayList<SimpleMusicTrack> createTrack(Scanner fileReader) {

        // Create an ArrayList of type SimpleMusicTrack.
        ArrayList<SimpleMusicTrack> list = new ArrayList<SimpleMusicTrack>();

        // Create a new SimpleMusicTrack Object.
        SimpleMusicTrack musicTrack = new SimpleMusicTrack();

        // While loop to set name, artist and album from file and add to the list.
        int i = 1;
        while (fileReader.hasNext()) {
            if (i == 1) {
                // Setter for name
                musicTrack.setName(fileReader.nextLine());
                i++;

            } else if (i == 2) {
                // Setter for artist
                musicTrack.setArtist(fileReader.nextLine());
                i++;
            } else if (i == 3) {
                // Setter for album
                musicTrack.setAlbum(fileReader.nextLine());

                // Add the filled up music track to the list.
                list.add(musicTrack);

                // Clear up musicTrack
                musicTrack = new SimpleMusicTrack();

                // Set while loop iterator back to 1
                i = 1;
            }
        }

        return list;
    }

    /**
     * Void method to print each SimpleMusicTrack out to Console
     *
     * @param list
     */
    public static void printTracks(ArrayList<SimpleMusicTrack> list) {

        // Header
        System.out.println(
                "\n" + "Music Tracks in the List of SimpleMusicTrack objects");
        System.out.print(
                "---------------------------------------------------------------");

        // For-loop to get every SimpleMusicTrack
        for (int i = 0; i < list.size(); i++) {
            // Retrieve element from list
            SimpleMusicTrack track = list.get(i);

            // Print the information of the element to Console
            System.out.println("\n" + "Track Name: " + track.getName());
            System.out.println("Artist Name: " + track.getArtist());
            System.out.println("Album Name: " + track.getAlbum() + "\n");
        }

        // End note of sub-test
        System.out.println("End of Print using SimpleMusicTrack Class.");
        System.out.println("-------------------------------------------");

    }

    /**
     * SubMethod to check if two SimpleMusicTracks are equal
     *
     * @param list
     * @param track
     * @return boolean
     */
    public static boolean trackCheck(ArrayList<SimpleMusicTrack> list,
            SimpleMusicTrack track) {

        // Variable to store the presence of the track in the list.
        boolean trackPresent = false;

        // While-loop to check every track in the list with the track of our own.
        int i = 0;
        while ((i < list.size()) && (trackPresent == false)) {
            // Get element from the list
            SimpleMusicTrack trackFromList = list.get(i);

            // Use equals method from the SimpleMusicTrack class.
            trackPresent = track.equals(trackFromList);

            // Iteration
            i++;
        }
        return trackPresent;
    }

    /**
     * Print equality of two tracks using the toString method in
     * SimpleMusicTrack class.
     *
     * @param trackPresent
     * @param track
     * @return String
     */
    public static String printEquality(boolean trackPresent,
            SimpleMusicTrack track) {

        // String variable to hold the entire string that needs to be returned.
        String equality = "";

        // if-else statements to check the boolean outcome from the sub-method
        if (trackPresent == true) {
            // String to show tracks are equal
            equality = track.toString()
                    + " is present in the Playlist(inputFile)";
        } else {
            // String to show tracks are not equal
            equality = track.toString()
                    + " is not present in the Playlist(inputFile)";
        }
        return equality;
    }

    /**
     * Void method to test for equality and toString Methods
     *
     * @param list
     */
    public static void equalToTest(ArrayList<SimpleMusicTrack> list) {

        // Header
        System.out.println(
                "\n" + "This is a test for equality and toString method.");
        System.out.println(
                "----------------------------------------------------------");

        // Create Track 1
        SimpleMusicTrack track1 = new SimpleMusicTrack();
        track1.setName("Blue Suede Shoes");
        track1.setArtist("Elvis Presley");
        track1.setAlbum("Elvis Presley: Legacy Edition");

        // Create Track 2
        SimpleMusicTrack track2 = new SimpleMusicTrack();
        track2.setName("Moonlight");
        track2.setArtist("XXXTentacion");
        track2.setAlbum("Na");

        // Check if the tracks is present in our list of SimpleMusicTrack
        boolean track1Present = trackCheck(list, track1);
        boolean track2Present = trackCheck(list, track2);

        // Print tracks status of quality.
        System.out.println(printEquality(track1Present, track1));

        System.out.println("");

        System.out.println(printEquality(track2Present, track2));

        System.out.println("");

        // End note of the sub-test
        System.out.println("End Of Equality and String Testing.");
        System.out.println("-------------------------------------");

    }

    /**
     * Void Method to print end of the test.
     */
    public static void endTest() {
        System.out.println("\n" + "-------------------------------------");
        System.out.println("End Of SimpleMusicTrack Test.       |");
        System.out.println("Test: Success                       |");
        System.out.println("-------------------------------------");
    }

    /**
     * Main Method
     *
     * @param arg
     */
    public static void main(String[] arg) {

        // Ask the client for the input file's name
        System.out.print(
                "Please enter the name of the file with the (.txt) ext.: ");
        Scanner in = new Scanner(System.in);
        File fileName = new File(in.nextLine());

        // Try-Catch to handle file I/O Exceptions
        try {
            // Read the file for printing using SimpleMusicTrack class
            Scanner fileReader = new Scanner(fileName);
            // A copy of the fileReader for equalTo test
            Scanner fileReaderCopy = new Scanner(fileName);

            // Create a copy of the tracks to cross check with a SimpleMusicTrack methods.
            createTrackFromFile(fileReader);

            // Check for next line in the file.
            // Order listed : Track --> Artist --> Album

            // Initialize a new ArrayList of SimpleMusicTrack
            ArrayList<SimpleMusicTrack> list = createTrack(fileReaderCopy);

            // Print the Tracks using SimpleMusicTrack Class/Object.
            printTracks(list);

            // Void method to test the equals method and toString method.
            equalToTest(list);

            // End of the test -- Print Statements.
            endTest();

            // Close i/o Streams
            fileReaderCopy.close();
            fileReader.close();

        } catch (IOException e) {
            // Throws an Exception if file is not found.
            System.err.println("FILE DOES NOT EXIST!!" + "\n");
            System.err.println("Test: ");
            System.err.println("Failed");

        }

        // Close i/o Streams.
        in.close();

    }
}
