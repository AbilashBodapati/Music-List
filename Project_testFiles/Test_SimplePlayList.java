import java.io.File;
import java.io.IOException;
import java.util.Scanner;

/**
 * This is a test java file to check if the class SimplePlayList works properly.
 *
 * @author Abilash Bodapati
 *
 */
public class Test_SimplePlayList {

    /**
     * Void method to print out the tracks and its info from the file.
     *
     * @param fileReader
     */
    public static void createTrackFromFile(Scanner fileReader) {

        // Header
        System.out.println("\n" + "Music Tracks from the File");
        System.out.println("--------------------------------");

        // While loop to help print out each track
        int i = 1;
        while (fileReader.hasNext()) {
            System.out.println(fileReader.nextLine());

            if (i == 3) {
                System.out.println("");
                i = 0;
            }

            i++;
        }

        // End note of the sub-test
        System.out.println("End of tracks from the file");
        System.out.println("-------------------------------------" + "\n");

    }

    /**
     * Void Method to help add the information into the list
     *
     * @param list
     */
    public static void addTracksInList(SimplePlayList list,
            Scanner fileReader) {

        // Create a new SimpleMusicTrack Object with type PlayListTrack
        PlayListTrack track = new SimpleMusicTrack();

        // While-loop to get track info and add into @code[track],
        //      then into @code[list]
        int i = 1;
        while (fileReader.hasNext()) {
            // Gets the next line;
            String line = fileReader.nextLine();

            // Checks which information line we are getting from file.
            // 1 = track name, 2 = track artist, 3 = track album
            if (i == 1) {
                // Track name
                track.setName(line);
                i++;
            } else if (i == 2) {
                // Track artist
                track.setArtist(line);
                i++;
            } else if (i == 3) {
                // Track album
                track.setAlbum(line);

                // Add the full track into the list
                list.addTrack(track);

                // Clear track for new track info
                track = new SimpleMusicTrack();

                // Start over with info retrieval process iteration.
                i = 1;
            }
        }
    }

    /**
     *
     * @param list
     * @return integer - size
     */
    public static int listSize(SimplePlayList list) {
        int size = 0;
        while (!list.isEmpty()) {
            list.getNextTrack();
            size++;
        }

        return size;
    }

    /**
     *
     * @param list
     */
    public static void remainingTracks(SimplePlayList list) {
        System.out.println("Remaining Tracks");
        System.out.println("--------------------");

        while (!list.isEmpty()) {
            System.out.println(list.getNextTrack().toString());
        }
        if (list.isEmpty()) {
            System.out.println("\n" + "List is now empty");
        }
    }

    /**
     * Void method to print each track out to Console using SimplePlayList
     *
     * @param list
     */
    public static void printTracks(SimplePlayList list, SimplePlayList list2) {

        // Header
        System.out.println(
                "\n" + "Music Tracks in the List of SimplePlayList objects");
        System.out.println(
                "---------------------------------------------------------------");

        System.out.println("There are " + listSize(list2) + " elements.");

        System.out.print(
                "Which Element do you want from the list [Enter an integer]: ");
        Scanner in = new Scanner(System.in);
        String elementPos = in.nextLine();
        int element = Integer.parseInt(elementPos);

        for (int i = 0; i < element - 1; i++) {
            PlayListTrack track = list.getNextTrack();
            list.addTrack(track);
        }

        System.out.print("\n"
                + "Do you want to take a peek[P/p] at or retrieve[R/r] track: ");
        String response = in.nextLine();

        if (response.equals("p") || response.equals("P")) {

            PlayListTrack peeked = list.peekAtNextTrack();
            System.out.println(
                    "-------Track Peeked: " + peeked.toString() + "\n");

            remainingTracks(list);

        } else if (response.equals("r") || response.equals("R")) {

            PlayListTrack retrieved = list.getNextTrack();

            System.out.println(
                    "-------Track Retrieved: " + retrieved.toString() + "\n");

            remainingTracks(list);
        }

        System.out.println("");

        // End note of sub-test
        System.out.println("\n" + "End of Print using SimplePlayList Class.");
        System.out.println("-------------------------------------------");

        // Close i/o Stream
        in.close();

    }

    /**
     * Void Method to print end of the test.
     */
    public static void endTest() {
        System.out.println("\n" + "-------------------------------------");
        System.out.println("End Of SimplePlayList Test.         |");
        System.out.println("Test: Success                       |");
        System.out.println("-------------------------------------");
    }

    /**
     * Main Method
     *
     * @param arg
     */
    public static void main(String[] arg) {

        // Ask the client for the input file's name
        System.out.print(
                "Please enter the name of the file with the (.txt) ext.: ");
        Scanner in = new Scanner(System.in);
        File fileName = new File(in.nextLine());

        // Try-Catch to handle file I/O Exceptions
        try {
            // Read the file for printing using SimpleMusicTrack class
            Scanner fileReader = new Scanner(fileName);
            // A copy of the fileReader for equalTo test
            Scanner fileReaderCopy = new Scanner(fileName);
            //A second Copy of the FileReader.
            Scanner fileReaderCopy2 = new Scanner(fileName);

            // Create a copy of the tracks to cross check with a SimpleMusicTrack methods.
            createTrackFromFile(fileReader);

            // Check for next line in the file.
            // Order listed : Track --> Artist --> Album

            // Initialize a new SimplePlayList object and add track info
            SimplePlayList list = new SimplePlayList();
            addTracksInList(list, fileReaderCopy);

            SimplePlayList list2 = new SimplePlayList();
            addTracksInList(list2, fileReaderCopy2);

            // Print the Tracks using SimpleMusicTrack Class/Object.
            printTracks(list, list2);

            // End of the test -- Print Statements.
            endTest();

            // Close i/o Streams
            fileReaderCopy.close();
            fileReader.close();

        } catch (IOException e) {
            // Throws an Exception if file is not found.
            System.err.println("FILE DOES NOT EXIST!!" + "\n");
            System.err.println("Test: ");
            System.err.println("Failed");

        }

        // Close i/o Streams.
        in.close();

    }
}
